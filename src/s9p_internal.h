#ifndef __S9P_INTERNAL_H_
#define __S9P_INTERNAL_H_

#include <s9p/s9p.h>

uint16_t get_stat_size(s9p_stat *stat);

void serialize_9p_str(char **str, uint8_t **buf);
void serialize_9p_qid(s9p_qid *qid, uint8_t **buf);
void serialize_9p_stat(s9p_stat *stat, uint8_t **buf);
void serialize_u16(uint16_t u16, uint8_t **buf);
void serialize_u32(uint32_t u32, uint8_t **buf);
void serialize_u64(uint64_t u64, uint8_t **buf);

char *deserialize_9p_str(uint8_t **buf);
void deserialize_9p_qid(uint8_t **buf, s9p_qid *qid);
int deserialize_9p_stat(uint8_t **buf, s9p_stat *stat);
uint16_t deserialize_u16(uint8_t **buf);
uint32_t deserialize_u32(uint8_t **buf);
uint64_t deserialize_u64(uint8_t **buf);

#endif /* s9p_internal.h */
