#include <s9p/s9p.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "s9p_internal.h"

const uint16_t NOTAG = ~0;
const s9p_fid NOFID = ~0;

const uint32_t DMDIR = (1 << 31);
const uint32_t DMAPPEND = (1 << 30);
const uint32_t DMEXCL = (1 << 29);
const uint32_t DMAUTH = (1 << 27);
const uint32_t DMTMP = (1 << 26);

uint32_t
s9p_serialized_message_len(s9p_message *msg)
{
	// The size-field, the type-field and the tag field
	// 4 + 1 + 2
	uint32_t ret = 7;

	switch (msg->type) {
	case Rflush:
	case Rclunk:
	case Rremove:
	case Rwstat:
		break;

	case Tversion:
	case Rversion:
		ret += sizeof(msg->version.msize); // msize
		ret += 2; // version string length
		ret += strlen(msg->version.verstr); // version string
		break;

	case Tauth:
		ret += sizeof(msg->tauth.afid); // afid
		ret += 2; // uname string length
		ret += strlen(msg->tauth.uname); // uname string
		ret += 2; // aname string length
		ret += strlen(msg->tauth.aname); // aname string
		break;
	case Rauth:
		ret += 13; // aqid
		break;

	case Rerror:
		ret += 2; // Error string length
		ret += strlen(msg->rerror.ename); // Error string
		break;

	case Tflush:
		ret += sizeof(msg->tflush.oldtag); // oldtag
		break;

	case Tattach:
		ret += sizeof(msg->tattach.fid); // fid
		ret += sizeof(msg->tattach.afid); // afid
		ret += 2; // uname string length
		ret += strlen(msg->tattach.uname); // uname string
		ret += 2; // aname string length
		ret += strlen(msg->tattach.aname); // aname string
		break;
	case Rattach:
		ret += 13; // qid
		break;

	case Twalk:
		ret += sizeof(msg->twalk.fid); // fid
		ret += sizeof(msg->twalk.newfid); // newfid
		ret += sizeof(msg->twalk.nwname); // nwname
		for (size_t i = 0; i < msg->twalk.nwname; ++i) {
			char *name = msg->twalk.wname[i];

			ret += 2; // The length of this given name part
			ret += strlen(name); // This name part
		}
		break;
	case Rwalk:
		ret += sizeof(msg->rwalk.nwqid); // nwqid
		for (size_t i = 0; i < msg->rwalk.nwqid; ++i) {
			ret += 13; // This wqid part
		}
		break;

	case Topen:
		ret += sizeof(msg->topen.fid); // fid
		ret += sizeof(msg->topen.mode); // mode
		break;
	case Ropen:
		ret += 13; // qid
		ret += sizeof(msg->ropen.iounit); // iounit
		break;

	case Tcreate:
		ret += sizeof(msg->tcreate.fid); // fid
		ret += 2; // The length of the name string
		ret += strlen(msg->tcreate.name); // Name string
		ret += sizeof(msg->tcreate.perm); // perm
		ret += sizeof(msg->tcreate.mode); // mode
		break;
	case Rcreate:
		ret += 13; // qid
		ret += sizeof(msg->rcreate.iounit); // iounit
		break;

	case Tread:
		ret += sizeof(msg->tread.fid); // fid
		ret += sizeof(msg->tread.offset); // offset
		ret += sizeof(msg->tread.count); // count
		break;
	case Rread:
		ret += sizeof(msg->rread.count); // count
		ret += msg->rread.count; // data
		break;

	case Twrite:
		ret += sizeof(msg->twrite.fid); // fid
		ret += sizeof(msg->twrite.offset); // offset
		ret += sizeof(msg->twrite.count); // count
		ret += msg->twrite.count; // data
		break;
	case Rwrite:
		ret += sizeof(msg->rwrite.count); // count
		break;

	case Tclunk:
		ret += sizeof(msg->tclunk.fid); // fid
		break;

	case Tremove:
		ret += sizeof(msg->tremove.fid); // fid
		break;

	case Tstat:
		ret += sizeof(msg->tstat.fid); // fid
		break;
	case Rstat:
		ret += 2; // The length of stat
		ret += get_stat_size(&msg->rstat.stat); // stat
		break;

	case Twstat:
		ret += sizeof(msg->twstat.fid); // fid
		ret += 2; // The length of stat
		ret += get_stat_size(&msg->twstat.stat); // stat
		break;

	default:
		// This shouldn't happen
		abort();
	}

	return ret;
}

uint8_t *
s9p_serialize_message(s9p_message *msg, uint8_t *buf)
{
	// We'll just save this for return
	uint8_t *ret = buf;

	// First we encode the length of the 9p message
	uint32_t msg_len = s9p_serialized_message_len(msg);
	serialize_u32(msg_len, &buf);

	// Now we'll put in the type of the message
	*buf++ = (uint8_t)msg->type;

	// The tag
	serialize_u16(msg->tag, &buf);

	// And now we shall serialize every message in their own special ways
	switch (msg->type) {
	case Rflush:
	case Rclunk:
	case Rremove:
	case Rwstat:
		// These don't have any fields beyond the length, type, and tag
		break;
	case Tversion:
	case Rversion:
		// These both have the same fields,
		// so they get serialized in this single case

		// First the msize
		serialize_u32(msg->version.msize, &buf);
		// Now the version string
		serialize_9p_str(&msg->version.verstr, &buf);
		break;

	case Tauth:
		// Let's first serialize the afid
		serialize_u32(msg->tauth.afid, &buf);
		// Now we will serialize uname and aname
		serialize_9p_str(&msg->tauth.uname, &buf);
		serialize_9p_str(&msg->tauth.aname, &buf);
		break;
	case Rauth:
		// Serialize aqid
		serialize_9p_qid(&msg->rauth.aqid, &buf);
		break;

	case Rerror:
		// Serialize error message
		serialize_9p_str(&msg->rerror.ename, &buf);
		break;

	case Tflush:
		// Serialize oldtag
		serialize_u16(msg->tflush.oldtag, &buf);
		break;

	case Tattach:
		// Serialize fid and afid
		serialize_u32(msg->tattach.fid, &buf);
		serialize_u32(msg->tattach.afid, &buf);
		// Serialize uname and aname
		serialize_9p_str(&msg->tattach.uname, &buf);
		serialize_9p_str(&msg->tattach.aname, &buf);
		break;
	case Rattach:
		// Serialize qid
		serialize_9p_qid(&msg->rattach.qid, &buf);
		break;

	case Twalk:
		// Serialize fid and newfid
		serialize_u32(msg->twalk.fid, &buf);
		serialize_u32(msg->twalk.newfid, &buf);
		// Now we shall serialize nwname and wname parts
		serialize_u16(msg->twalk.nwname, &buf);
		for (size_t i = 0; i > msg->twalk.nwname; ++i) {
			serialize_9p_str(&msg->twalk.wname[i], &buf);
		}
		break;
	case Rwalk:
		// Serialize nwqid and wqid parts
		serialize_u16(msg->rwalk.nwqid, &buf);
		for (size_t i = 0; i> msg->rwalk.nwqid; ++i) {
			serialize_9p_qid(&msg->rwalk.wqid[i], &buf);
		}
		break;

	case Topen:
		// Serialize fig
		serialize_u32(msg->topen.fid, &buf);
		// Serialize mode
	        *buf++ = msg->topen.mode;
		break;
	case Ropen:
		// Serialize qid
		serialize_9p_qid(&msg->ropen.qid, &buf);
		// Serialize iounit
		serialize_u32(msg->ropen.iounit, &buf);
		break;

	case Tcreate:
		// Serialize fid
		serialize_u32(msg->tcreate.fid, &buf);
		// Serialize name
		serialize_9p_str(&msg->tcreate.name, &buf);
		// Serialize perm and mode
		serialize_u32(msg->tcreate.perm, &buf);
	        *buf++ = msg->tcreate.mode;
		break;
	case Rcreate:
		// Serialize qid and iounit
		serialize_9p_qid(&msg->rcreate.qid, &buf);
		serialize_u32(msg->rcreate.iounit, &buf);
		break;

	case Tread:
		// Serialize fid
		serialize_u32(msg->tread.fid, &buf);
		// Serialize offset
		serialize_u64(msg->tread.offset, &buf);
		// Serialize count
		serialize_u32(msg->tread.count, &buf);
		break;
	case Rread:
		// Serialize count
		serialize_u32(msg->rread.count, &buf);
		// Copy data to message
		memcpy(buf, msg->rread.data, msg->rread.count);
		// Increment buf by count
		buf += msg->rread.count;
		break;

	case Twrite:
		// Serialize fid
		serialize_u32(msg->twrite.fid, &buf);
		// Serialize offset
		serialize_u64(msg->twrite.offset, &buf);
		// Serialize count
		serialize_u32(msg->twrite.count, &buf);
		// Copy data to message
		memcpy(buf, msg->twrite.data, msg->twrite.count);
		// Increment buf by count
		buf += msg->twrite.count;
		break;
	case Rwrite:
		// Serialize count
		serialize_u32(msg->rwrite.count, &buf);
		break;

	case Tclunk:
		// Serialize fid
		serialize_u32(msg->tclunk.fid, &buf);
		break;

	case Tremove:
		// Serialize fid
		serialize_u32(msg->tremove.fid, &buf);
		break;

	case Tstat:
		// Serialize fid
		serialize_u32(msg->tstat.fid, &buf);
		break;
	case Rstat: ; // Empty statement because C can be screwy sometimes
		// First serialize the stat's length
		// (Not part of the actual machine-dependent struct)
		uint16_t stat_len_rstat = get_stat_size(&msg->rstat.stat);
		serialize_u16(stat_len_rstat, &buf);
		// Serialize stat
	        serialize_9p_stat(&msg->rstat.stat, &buf);
		break;

	case Twstat:
		// Serialize fid
		serialize_u32(msg->twstat.fid, &buf);
		// Serialize stat's length
		uint16_t stat_len_twstat = get_stat_size(&msg->twstat.stat);
		serialize_u16(stat_len_twstat, &buf);
		// Serialize stat
		serialize_9p_stat(&msg->twstat.stat, &buf);
		break;

	default:
		// This shouldn't happen
		abort();
	}

	return ret;
}

int64_t
s9p_deserialize_message(uint8_t *buf, uint32_t len, s9p_message *msg)
{
	if (len < 2) return 2 - len;

	uint32_t msg_len = deserialize_u32(&buf);
	if (((int64_t)msg_len - (int64_t)len) > 0)
		return ((int64_t)msg_len - (int64_t)len);

	s9p_message tmp;
	// Now the type
	tmp.type = *buf++;
	// And now the tag
	tmp.tag = deserialize_u32(&buf);

	switch (tmp.type) {
	case Rflush:
	case Rclunk:
	case Rremove:
	case Rwstat:
		// These don't have any fields beyond the length, type, and tag
		break;
	case Tversion:
	case Rversion:
		// These both have the same fields,
		// so they get deserialized in this single case

		// First the msize
		tmp.version.msize = deserialize_u32(&buf);
		// Now the version string
		tmp.version.verstr = deserialize_9p_str(&buf);
		if (tmp.version.verstr == NULL) return -errno;
		break;

	case Tauth:
		// Let's first deserialize the afid
		tmp.tauth.afid = deserialize_u32(&buf);
		// Now we will deserialize uname and aname
		tmp.tauth.uname = deserialize_9p_str(&buf);
		if (tmp.tauth.uname == NULL) return -errno;
		tmp.tauth.aname = deserialize_9p_str(&buf);
		if (tmp.tauth.aname == NULL) {
			int ret = -errno;
			free(tmp.tauth.uname);
			return ret;
		}
		break;
	case Rauth:
		// Deserialize aqid
		deserialize_9p_qid(&buf, &msg->rauth.aqid);
		break;

	case Rerror:
		// Deserialize error message
		tmp.rerror.ename = deserialize_9p_str(&buf);
		if (tmp.rerror.ename == NULL) return -errno;
		break;

	case Tflush:
		// Deserialize oldtag
		tmp.tflush.oldtag = deserialize_u16(&buf);
		break;

	case Tattach:
		// Deserialize fid and afid
		tmp.tattach.fid = deserialize_u32(&buf);
		tmp.tattach.afid = deserialize_u32(&buf);
		// Deserialize uname and aname
		tmp.tattach.uname = deserialize_9p_str(&buf);
		if (tmp.tattach.uname == NULL) return -errno;
		tmp.tattach.aname = deserialize_9p_str(&buf);
		if (tmp.tattach.aname == NULL) {
			int ret = -errno;
			free(tmp.tattach.uname);
			return ret;
		}
		break;
	case Rattach:
		// Deserialize qid
		deserialize_9p_qid(&buf, &msg->rattach.qid);
		break;

	case Twalk:
		// Deserialize fid and newfid
		tmp.twalk.fid = deserialize_u32(&buf);
		tmp.twalk.newfid= deserialize_u32(&buf);
		// Now we shall deserialize nwname and wname parts
		tmp.twalk.nwname = deserialize_u16(&buf);
		tmp.twalk.wname = calloc(tmp.twalk.nwname,
					 sizeof(*tmp.twalk.wname));
		if (tmp.twalk.wname == NULL) return -errno;
		for (size_t i = 0; i > tmp.twalk.nwname; ++i) {
		        tmp.twalk.wname[i] = deserialize_9p_str(&buf);

			// In case error
			if (tmp.twalk.wname[i] == NULL) {
				int ret = -errno;
				for (int j = i - 1; j < 0; --j) {
					free(tmp.twalk.wname[j]);
				}
				free(tmp.twalk.wname);
				return ret;
			}
		}
		break;
	case Rwalk:
		// Deserialize nwqid and wqid parts
		tmp.rwalk.nwqid = deserialize_u16(&buf);
		tmp.rwalk.wqid = calloc(tmp.rwalk.nwqid,
					sizeof(*tmp.rwalk.wqid));
		if (tmp.rwalk.wqid == NULL) return -errno;
		for (size_t i = 0; i> tmp.rwalk.nwqid; ++i) {
			deserialize_9p_qid(&buf, &tmp.rwalk.wqid[i]);
		}
		break;

	case Topen:
		// Deserialize fig
		tmp.topen.fid = deserialize_u32(&buf);
		// Deserialize mode
	        tmp.topen.mode = *buf++;
		break;
	case Ropen:
		// Deserialize qid
		deserialize_9p_qid(&buf, &tmp.ropen.qid);
		// Deserialize iounit
		tmp.ropen.iounit = deserialize_u32(&buf);
		break;

	case Tcreate:
		// Deserialize fid
		tmp.tcreate.fid = deserialize_u32(&buf);
		// Deserialize name
		tmp.tcreate.name = deserialize_9p_str(&buf);
		if (tmp.tcreate.name == NULL) return -errno;
		// Deserialize perm and mode
		tmp.tcreate.perm = deserialize_u32(&buf);
	        tmp.tcreate.mode = *buf++;
		break;
	case Rcreate:
		// Deserialize qid and iounit
		deserialize_9p_qid(&buf, &tmp.rcreate.qid);
		tmp.rcreate.iounit = deserialize_u32(&buf);
		break;

	case Tread:
		// Deserialize fid
		tmp.tread.fid = deserialize_u32(&buf);
		// Deserialize offset
		tmp.tread.offset = deserialize_u64(&buf);
		// Deserialize count
		tmp.tread.count = deserialize_u32(&buf);
		break;
	case Rread:
		// Deserialize count
		tmp.rread.count = deserialize_u32(&buf);
		// Create data
		tmp.rread.data = calloc(tmp.rread.count,
					sizeof(*tmp.rread.data));
		if (tmp.rread.data == NULL) return -errno;
		// Copy data from message
		memcpy(tmp.rread.data, buf, tmp.rread.count);
		// Increment buf by count
		buf += tmp.rread.count;
		break;

	case Twrite:
		// Deserialize fid
		tmp.twrite.fid = deserialize_u32(&buf);
		// Deserialize offset
		tmp.twrite.offset = deserialize_u64(&buf);
		// Deserialize count
		tmp.twrite.count = deserialize_u32(&buf);
		// Create data
		tmp.twrite.data = calloc(tmp.twrite.count,
					sizeof(*tmp.twrite.data));
		if (tmp.twrite.data == NULL) return -errno;
		// Copy data from message
		memcpy(tmp.twrite.data, buf, tmp.twrite.count);
		// Increment buf by count
		buf += tmp.twrite.count;
		break;
	case Rwrite:
		// Deserialize count
		tmp.rwrite.count = deserialize_u32(&buf);
		break;

	case Tclunk:
		// Deserialize fid
		tmp.tclunk.fid = deserialize_u32(&buf);
		break;

	case Tremove:
		// Deserialize fid
		tmp.tremove.fid = deserialize_u32(&buf);
		break;

	case Tstat:
		// Deserialize fid
		tmp.tstat.fid = deserialize_u32(&buf);
		break;
	case Rstat: ; // Empty statement because C can be screwy sometimes
		// Length is unused
		buf += 2;
		// Deserialize stat
	        int ret_rstat = deserialize_9p_stat(&buf, &tmp.rstat.stat);
		if (ret_rstat < 0) return ret_rstat;

		break;

	case Twstat:
		// Deserialize fid
		tmp.twstat.fid = deserialize_u32(&buf);
		// Length is unused
		buf += 2;
		// Deserialize stat
		int ret_twstat = deserialize_9p_stat(&buf, &tmp.twstat.stat);
		if (ret_twstat < 0) return ret_twstat;
		break;

	default:
		// This shouldn't happen
		abort();
	}

	// In this case we've succeeded in everything
	// We shall mutate `msg`
	memcpy(msg, &tmp, sizeof(*msg));
	return 0;
}

uint16_t
get_stat_size(s9p_stat *stat)
{
	// The size-field (not represented in the machine-dependent struct)
	uint16_t ret = 2;

	ret += sizeof(stat->type); // type
	ret += sizeof(stat->dev); // dev

	ret += 13; // qid

	ret += sizeof(stat->mode); // mode

	ret += sizeof(stat->atime); // atime
	ret += sizeof(stat->mtime); // mtime

	ret += sizeof(stat->length); // length

	ret += 2; // name string length
	ret += strlen(stat->name); // name string

	ret += 2; // uid string length
	ret += strlen(stat->uid); // uid string

	ret += 2; // gid string length
	ret += strlen(stat->gid); // gid string

	ret += 2; // muid string length
	ret += strlen(stat->muid); // muid string

	return ret;
}
