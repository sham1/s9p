#ifndef S9P_S9P_H
#define S9P_S9P_H

#include <stdint.h>
#include <stddef.h>

typedef uint32_t s9p_fid;

typedef enum
{
	Tversion	=	100,
	Rversion,

	Tauth		=	102,
	Rauth,

	Tattach		=	104,
	Rattach,

	// Signifies an error.
	// Note that unlike every other message type,
	// this has no corresponding T variant, i.e. no Terror.
	Rerror		=	107,

	Tflush		=	108,
	Rflush,

	Twalk		=	110,
	Rwalk,

	Topen		=	112,
	Ropen,

	Tcreate		=	114,
	Rcreate,

	Tread		=	116,
	Rread,

	Twrite		=	118,
	Rwrite,

	Tclunk		=	120,
	Rclunk,

	Tremove		=	122,
	Rremove,

	Tstat		=	124,
	Rstat,

	Twstat		=	126,
	Rwstat,

	Tmax,
} s9p_message_type;

extern const uint16_t NOTAG;
extern const s9p_fid NOFID;

// This represents the 9p server's unique identification of the file.
typedef struct
{
	uint8_t type;
	uint32_t version;
	uint64_t identifier;
} s9p_qid;

// This represents the file's directory entry.
typedef struct
{
	uint16_t type; // Used by Plan9 kernel.
	uint32_t dev; // Used by Plan9 kernel.

	s9p_qid qid; // The qid associated with this directory entry.

	uint32_t mode; // File's access mode.
	uint32_t atime; // Last access time.
	uint32_t mtime; // Last modification time.

	uint64_t length; // The length of the file.

	char *name; // File name
	char *uid; // Owner name
	char *gid; // Group name
	char *muid; // Name of the user who last modified file.
} s9p_stat;

// This represents a machine-dependent version of a given 9p message.
// Note that this must be serialized with `s9p_serialize_message`
// before sending.
typedef struct
{
	s9p_message_type type;
	uint16_t tag;
	union {
		struct {
			// Maximum size of data to be transfered in
			// a single 9p message.
			uint32_t msize;

			// Usually `9P2000`
			char *verstr;
		} version; // Used by both Tversion and Rversion

		struct {
			s9p_fid afid;
		        char *uname;
			char *aname;
		} tauth;
		struct {
			s9p_qid aqid;
		} rauth;

		struct {
			char *ename;
		} rerror;

		struct {
			uint16_t oldtag;
		} tflush;

		struct {
		        s9p_fid fid;
		        s9p_fid afid;
			char *uname;
			char *aname;
		} tattach;
		struct {
			s9p_qid qid;
		} rattach;

		struct {
		        s9p_fid fid;
		        s9p_fid newfid;
			uint16_t nwname;
			char **wname;
		} twalk;
		struct {
			uint16_t nwqid;
			s9p_qid *wqid;
		} rwalk;

		struct {
		        s9p_fid fid;
			uint8_t mode;
		} topen;
		struct {
			s9p_qid qid;
			uint32_t iounit;
		} ropen;

		struct {
		        s9p_fid fid;
			char *name;
			uint32_t perm;
			uint8_t mode;
		} tcreate;
		struct {
			s9p_qid qid;
			uint32_t iounit;
		} rcreate;

		struct {
		        s9p_fid fid;
			uint64_t offset;
			uint32_t count;
		} tread;
		struct {
			uint32_t count;
			uint8_t *data;
		} rread;

		struct {
		        s9p_fid fid;
			uint64_t offset;
			uint32_t count;
			uint8_t *data;
		} twrite;
		struct {
			uint32_t count;
		} rwrite;

		struct {
		        s9p_fid fid;
		} tclunk;

		struct {
		        s9p_fid fid;
		} tremove;

		struct {
		        s9p_fid fid;
		} tstat;
		struct {
			s9p_stat stat;
		} rstat;

		struct {
		        s9p_fid fid;
			s9p_stat stat;
		} twstat;
	};
} s9p_message;

typedef enum
{
	OWNER_READ = (1 << 8),
	OWNER_WRITE = (1 << 7),
	OWNER_EXECUTE = (1 << 6),

	GROUP_READ = (1 << 5),
        GROUP_WRITE = (1 << 4),
        GROUP_EXECUTE = (1 << 3),

        OTHER_READ = (1 << 2),
	OTHER_WRITE = (1 << 1),
        OTHER_EXECUTE = (1 << 0),
} s9p_permissions;

// These are a part of the permission octet

// Indicates that this file is actually a directory
// Directories cannot be written into by using the
// write-message.
extern const uint32_t DMDIR;

// This file is append-only, as in on write the offset is ignored
// and the content is appended at the end of the file.
extern const uint32_t DMAPPEND;

// This file has exclusive access, as in only one fid is allowed
// to have this open across all clients. (i.e. only one client
// is allowed access)
extern const uint32_t DMEXCL;

// This file is used on the authentication process.
// Authentication is not specified in the 9p protocol
// and is application-defined.
extern const uint32_t DMAUTH;

// This file or directory is temporary.
extern const uint32_t DMTMP;

enum
{
	OREAD = 0,
	OWRITE = 1,
	ORDWR = 2,
	OEXEC = 3,

	OTRUNC = 0x10,
	ORCLOSE = 0x40,
	OEXCL = 0x1000,
};

// This function returns the length of the buffer
// needed for storing a serialized `s9p_message`
// in bytes.
uint32_t s9p_serialized_message_len(s9p_message *msg);

// This function serializes the given message `msg` and puts the serialized
// message into a given buffer `buf`. Note that `buf` has to have the length
// given by `s9p_serialized_message_len`.
uint8_t *s9p_serialize_message(s9p_message *msg, uint8_t *buf);

// This function recreates contents of `msg` from `buf` of size `len`.
//
// This function returns 0 on success, >0 if the `len` is less than what the
// message informs (for instance one should read return value count of bytes
// from the network and add them to the buffer), and <0 in case of an allocation
// error (negative errno). `msg` will only be modified on success.
//
// NOTE: in the resulting `msg` all pointers' ownerships are to be on the user.
// This means that the user is responsible for deallocating any given pointer.
int64_t s9p_deserialize_message(uint8_t *buf, uint32_t len, s9p_message *msg);

#endif /* s9p/s9p.h */
