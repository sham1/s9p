#include <s9p/s9p.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "s9p_internal.h"

void
serialize_9p_str(char **str, uint8_t **buf)
{
	char *s = *str;
	uint8_t *b = *buf;

	size_t slen = strlen(s);
	// From context we already know that the string is going to be
	// at most 2^16 long, because that's the maximum length allowed
	// for strings that are part of the actual message instead of payload.
	uint16_t slen_actual = slen;
	serialize_u16(slen_actual, &b);

	// I can only assume that the buffer and the string don't overlap...
	memcpy(b, s, slen_actual);

	// And now we just advance the buffer pointer
	b += slen_actual;
}

void
serialize_9p_qid(s9p_qid *qid, uint8_t **buf)
{
	uint8_t *b = *buf;

	*b++ = qid->type;
	serialize_u32(qid->version, &b);
	serialize_u64(qid->identifier, &b);
}

void
serialize_9p_stat(s9p_stat *stat, uint8_t **buf)
{
	uint8_t *b = *buf;
	// The size-field
	// (not present on non-serialized struct)
	// We subtract 2 because this field doesn't
	// include itself
	uint16_t size = get_stat_size(stat) - 2;
	serialize_u16(size, &b);

	// Serialize type and dev
	serialize_u16(stat->type, &b);
	serialize_u32(stat->dev, &b);

	// Serialize mode, atime, and mtime
	serialize_u32(stat->mode, &b);
	serialize_u32(stat->atime, &b);
	serialize_u32(stat->mtime, &b);

	// Serialize length
	serialize_u64(stat->length, &b);

	// Serialize name, uid, gid, and muid
	serialize_9p_str(&stat->name, &b);
	serialize_9p_str(&stat->uid, &b);
	serialize_9p_str(&stat->gid, &b);
	serialize_9p_str(&stat->muid, &b);
}

void
serialize_u16(uint16_t u16, uint8_t **buf)
{
	uint8_t *b = *buf;
	*b++ = (uint8_t)((u16 >> 0)  & 0xFF);
	*b++ = (uint8_t)((u16 >> 8)  & 0xFF);
}

void
serialize_u32(uint32_t u32, uint8_t **buf)
{
	uint8_t *b = *buf;
	*b++ = (uint8_t)((u32 >> 0)  & 0xFF);
	*b++ = (uint8_t)((u32 >> 8)  & 0xFF);
	*b++ = (uint8_t)((u32 >> 16) & 0xFF);
	*b++ = (uint8_t)((u32 >> 24) & 0xFF);
}

void
serialize_u64(uint64_t u64, uint8_t **buf)
{
	uint8_t *b = *buf;
	*b++ = (uint8_t)((u64 >> 0)  & 0xFF);
	*b++ = (uint8_t)((u64 >> 8)  & 0xFF);
	*b++ = (uint8_t)((u64 >> 16) & 0xFF);
	*b++ = (uint8_t)((u64 >> 24) & 0xFF);
	*b++ = (uint8_t)((u64 >> 32) & 0xFF);
	*b++ = (uint8_t)((u64 >> 40) & 0xFF);
	*b++ = (uint8_t)((u64 >> 48) & 0xFF);
	*b++ = (uint8_t)((u64 >> 56) & 0xFF);
}

char *
deserialize_9p_str(uint8_t **buf)
{
	uint8_t *b = *buf;
	uint16_t len = deserialize_u16(&b);

	// We will allocate one more than the length
	// so we may get a NUL at the end
	char *ret = calloc(len + 1, sizeof(*ret));
	if (ret == NULL) return NULL;

	memcpy(ret, b, len);
	b += len;
	return ret;
}

void
deserialize_9p_qid(uint8_t **buf, s9p_qid *qid)
{
	uint8_t *b = *buf;
	qid->type = *b++;
	qid->version = deserialize_u32(&b);
	qid->identifier = deserialize_u64(&b);
}

int
deserialize_9p_stat(uint8_t **buf, s9p_stat *stat)
{
	int ret = 0;
	uint8_t *b = *buf;
	// We'll skip the length because we don't need it here
	b += 2;

	// Type and dev
	stat->type = deserialize_u16(&b);
	stat->dev = deserialize_u32(&b);

	// Qid
	deserialize_9p_qid(&b, &stat->qid);

	// mode, atime, mtime
	stat->mode = deserialize_u32(&b);
	stat->atime = deserialize_u32(&b);
	stat->mtime = deserialize_u32(&b);

	// length
	stat->length = deserialize_u64(&b);

	// name, uid, gid, muid
	if ((stat->name = deserialize_9p_str(&b)) == NULL) {
		ret = -errno;
		goto err;
	}

	if ((stat->uid = deserialize_9p_str(&b)) == NULL) {
		ret = -errno;
		goto clean_name;
	}

	if ((stat->gid = deserialize_9p_str(&b)) == NULL) {
		ret = -errno;
		goto clean_uid;
	}

	if ((stat->muid = deserialize_9p_str(&b)) == NULL) {
		ret = -errno;
		goto clean_gid;
	}

	return 0;

clean_gid:
	free(stat->gid);
clean_uid:
	free(stat->uid);
clean_name:
	free(stat->name);
err:
	return ret;
}

uint16_t
deserialize_u16(uint8_t **buf)
{
	uint8_t *b = *buf;
        uint16_t ret =
		(b[0] << 0)|
		(b[1] << 8);

	b += 2;
	return ret;
}

uint32_t
deserialize_u32(uint8_t **buf)
{
	uint8_t *b = *buf;
	uint32_t ret =
		(b[0] << 0) |
		(b[1] << 8) |
		(b[2] << 16)|
		(b[3] << 24);

	b += 4;
	return ret;
}

uint64_t
deserialize_u64(uint8_t **buf)
{
	uint8_t *b = *buf;
	uint64_t ret =
		((uint64_t)b[0] << 0) |
		((uint64_t)b[1] << 8) |
		((uint64_t)b[2] << 16)|
		((uint64_t)b[3] << 24)|
		((uint64_t)b[4] << 32)|
		((uint64_t)b[5] << 40)|
		((uint64_t)b[6] << 48)|
		((uint64_t)b[7] << 56);

	b += 8;
	return ret;
}
