s9p (Simple 9p)
===============

This library is a simple implementation of
[the 9p file protocol.](http://9p.cat-v.org/)

This library is still work-in-progress. The features currently implemented:

* Translating between messages and the wire format

License
-------

MIT
